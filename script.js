//"use strict";
let tasks = [];

const getPriorityName = function (priority) {
  switch (priority) {
    case "1":
      return "High";
    case "2":
      return "Medium";
    case "3":
      return "Low";
    default:
      return "";
  }
};

const deleteTask = function (i) {
  if (!confirm("Are you sure ?")) return;
  tasks.splice(i, 1);
  renderTable();
};
const moveUp = function (i) {
  if (i == 0) return;
  const oldTask = tasks[i];
  tasks[i] = tasks[i - 1];
  tasks[i - 1] = oldTask;
  renderTable();
};
const moveDown = function (i) {
  if (i == tasks.length - 1) return;
  const oldTask = tasks[i];
  tasks[i] = tasks[i + 1];
  tasks[i + 1] = oldTask;
  renderTable();
};

const renderTable = function () {
  const tbody = document.querySelector("#tasks_tbody");
  tbody.innerHTML = "";
  tasks.forEach((t, i) => {
    const row = `
        <tr>
        <td>${i + 1}</td>
        <td id="taskName_${i}">${t.name}</td>
        
        <td>${getPriorityName(t.priority)}</td>
        <td>
        ${
          i > 0
            ? `<button class="btn btn-sm btn-secondary" onclick="moveUp(${i})">Up</button>`
            : ``
        }
        ${
          i < tasks.length - 1
            ? `<button class="btn btn-sm btn-secondary" onclick="moveDown(${i})">Down</button>`
            : ``
        }
        </td>
        <td>
        <button class="btn btn-primary btn-sm" onclick="edit(${i})" >Edit</button>
        <button class="btn btn-success btn-sm" id="saveBtn_${i}" onclick="save(${i})" style="display:none;">Save</button>
        <button class="btn btn-danger btn-sm" id="cancelBtn_${i}" onclick="cancel()" style="display:none;">Cancel</button>
        <button class="btn btn-danger btn-sm" onclick="deleteTask(${i})">Delete</button></td>
        </tr>
        `;
    tbody.insertAdjacentHTML("beforeEnd", row);
  });
};
const addTask = function () {
  console.log(this);
  const taskName = document.querySelector("#task_name").value;
  const priority = document.querySelector("#task_priority").value;
  if (taskName !== "" && priority > 0) {
    tasks.push({
      name: taskName,
      priority: priority,
    });
    renderTable();
  }
};

document.querySelector("#add").addEventListener("click", addTask);
var name = "Test3";
var age = 22;
const calcFunction = () => {
  console.log(this);
  console.log(`My name is ${this.name} I'm ${this.age} years old`);
};
const obj = {
  name: "Test",
  age: 35,
  cal: calcFunction,
};

const obj2 = {
  name: "Test2",
  age: 22,
  cal: calcFunction,
};

function thisTest() {
  let obj1 = "Ramy";
  var obj2 = "Ahmed";
  console.log(this);
  const x = () => {
    console.log(this);
  };
  x();
}


edit = (i) => {
  document.getElementById(`saveBtn_${i}`).removeAttribute("style");
  document.getElementById(`cancelBtn_${i}`).removeAttribute("style");

  const taskNameField = document.querySelector(`#taskName_${i}`);
  taskNameField.innerHTML = '';
  
  const editedTaskName = `
  <input type="text" id="taskNameInput" class="form-control";/>
  `;
  taskNameField.insertAdjacentHTML("beforeEnd", editedTaskName);
}

const save = (i) => {
  document.getElementById(`saveBtn_${i}`).setAttribute("style", "display:none");
  document.getElementById(`cancelBtn_${i}`).setAttribute("style", "display:none");
  
  const task = document.querySelector("#taskNameInput");
  tasks[i].name = task.value;

  renderTable();
}

const cancel = () => {
  renderTable();
}